# [latex-resume][assets]

LaTeX Résumé repository that [builds][bpdfs] and [hosts][assets] the PDF files.

_Special thanks to [Logan Marchione](https://loganmarchione.com/) ([source][loganres]) and [Bill Ryan](https://www.yuanbin.me/) ([source][billres]) for providing the base templates!_

---
# Basic Usage

By default the `resume.cls` script uses Font Awesome 5 (FA5), but includes a `boolen` switch for Font Awesome 4 (FA4).  Add a `%` comment at the beginning of line [37](https://gitlab.com/Kovrinic/latex-resume/blob/master/resume.cls#L37) in `resume.cls` to use FA4 instead of FA5.  Differences can be seen below.

<table>
  <thead>
    <tr>
      <td align="center"><b>Font Awesome 4</b></td>
      <td align="center"><b>Font Awesome 5</b></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><img src="https://kovrinic.gitlab.io/latex-resume/Resume_MatthewRothfuss_FA4.png" /></td>
      <td><img src="https://kovrinic.gitlab.io/latex-resume/Resume_MatthewRothfuss_FA5.png" /></td>
    </tr>
    <tr>
      <td align="center"><a href="https://kovrinic.gitlab.io/latex-resume/Resume_MatthewRothfuss_FA4.pdf" title="FontAwesome4.pdf">FontAwesome4.pdf</a></td>
      <td align="center"><a href="https://kovrinic.gitlab.io/latex-resume/Resume_MatthewRothfuss.pdf" title="FontAwesome5.pdf">FontAwesome5.pdf</a></td>
    </tr>
  </tbody>
</table>

---
# Usage

TODO:
- how to build locally
- how to build with GitLab-CI

---

[assets]:https://kovrinic.gitlab.io/latex-resume/
[bpdfs]:https://gitlab.com/Kovrinic/latex-resume/blob/master/build-pdf-small.sh
[loganres]:https://loganmarchione.com/resume/
[billres]:https://github.com/billryan/resume
