%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Original by Logan Marchione on 02/13/2017
% https://loganmarchione.com
% https://loganmarchione.com/resume/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ubuntu packages
% sudo apt-get install texlive-fonts-extra texlive-fonts-recommended texlive-fontenc texlive-xetex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Arch packages
% sudo pacman -S texmaker texlive-most && updmap --enable Map=kpfonts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% XeLaTex is required for this, since we're using Unicode and custom fonts
\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{resume}[2017/02/13 Logan Marchione and 2018/09/02 Matthew Rothfuss]
\LoadClass{article}
\RequirePackage{geometry}                 % to change page size/orientation/margins
\RequirePackage{fontspec}                 % to add custom fonts (see below)
\RequirePackage{kpfonts}                  % to use KP Serif
\usepackage[T1]{fontenc}                  % to set font encoding (required by KP Serif)
\RequirePackage[document]{ragged2e}       % to left-align everything (using [document])
\RequirePackage{enumitem}                 % to change spacing between lists
  \setlist[itemize]{noitemsep,topsep=0pt} % set some list defaults
\RequirePackage{titlesec}                 % to format the title
\RequirePackage{color}                    % to define specific colors
\RequirePackage{tabto}                    % to align text to a certain point
  \newcommand{\rightsidetab}{4.85in}      % set the tab on the right side
  \newcommand{\skilltab}{1.6in}           % set tab for sill list
\RequirePackage{hyperref}                 % to make clickable URLs
\setlength\parindent{0pt}                 % disable indent globally
\RequirePackage{xifthen}                  % add if/then logic

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FontAwesome (4 or 5)      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newboolean{boolfa5}
\setboolean{boolfa5}{true}                % add comment to use version 4
\ifthenelse{\boolean{boolfa5}}{
  % https://ctan.org/pkg/fontawesome5
  \RequirePackage{fontawesome5}
    \newcommand{\CfaBriefcase}{\faIcon[solid]{briefcase}}
    \newcommand{\CfaCheck}{\faIcon[regular]{check-square}}
    \newcommand{\CfaHeart}{\faIcon[regular]{heart}}
    \newcommand{\CfaGraduationCap}{\faIcon[solid]{graduation-cap}}
    \newcommand{\CfaHome}{\faIcon[solid]{home}}
    \newcommand{\CfaPhone}{\faIcon[solid]{phone}}
    \newcommand{\CfaEnvelope}{\faIcon[solid]{envelope}}
    \newcommand{\CfaGlobe}{\faIcon[solid]{globe-americas}}
    \newcommand{\CfaMapMarker}{\faIcon[solid]{map-marker-alt}}
}{
  % workaround for fontawesome version 4 in XeLaTeX
  % https://github.com/xdanaux/fontawesome-latex/issues/12#issue-191541633
  \defaultfontfeatures{Extension = .otf}
  \RequirePackage{fontawesome}
    \newcommand{\CfaBriefcase}{\faBriefcase}
    \newcommand{\CfaCheck}{\faCheckSquareO}
    \newcommand{\CfaHeart}{\faHeartO}
    \newcommand{\CfaGraduationCap}{\faGraduationCap}
    \newcommand{\CfaHome}{\faHome}
    \newcommand{\CfaPhone}{\faPhone}
    \newcommand{\CfaEnvelope}{\faEnvelope}
    \newcommand{\CfaGlobe}{\faGlobe}
    \newcommand{\CfaMapMarker}{\faMapMarker}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colors                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{darkblue}{RGB}{6,69,173}      % custom color for URLs


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hyperlinks                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hypersetup{
  colorlinks=true,
  linkcolor=darkblue,                      % color of internal links
  urlcolor=darkblue,                       % color of external links
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Contact info              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\contactname}[1]{%
  {\Huge{#1}}\\
}

\newcommand{\contactinfophysical}[3]{%
  {\CfaHome} {#1}, {#2} {#3}\\
}

% phone number | usage: \phone{<phone>}
\newcommand{\phone}[2][]{%
  {\CfaPhone} \ifthenelse{\isempty{#1}}
  {\href{#2}{#2}}
  {\href{tel:+#2}{#1}}
}

% email | usage: \email{<email>}
\newcommand{\email}[1]{%
  {\CfaEnvelope} \href{mailto:#1}{#1}
}

% homepage | usage: \homepage{<homepage>}
\newcommand{\homepage}[2][]{%
  {\CfaGlobe} \ifthenelse{\isempty{#1}}
  {\href{#2}{#2}}
  {\href{#2}{#1}}
}

\newcommand{\contactinfodigital}[3]{%
  \vspace{0.9ex}
  {#1}  |  {#2}  |  {#3}\\
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sections                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\titleformat{\section}
  {\Large\scshape}
  {}{0em}
  {}
  [\titlerule]

% remove icons from section bookmarks
\newcommand{\UBSection}[2]{%
  \texorpdfstring{{#1}\space}{}{#2}
}

\newcommand{\placeandlocation}[2]{%
  {\large{\bf{#1}}} \tabto{\rightsidetab}{\CfaMapMarker} {#2}\\
}

% add space between jobs
\newcommand{\placeandlocationSpace}[2]{%
  \vspace{2ex}
  {\large{\bf{#1}}} \tabto{\rightsidetab}{\CfaMapMarker} {#2}\\
}

\newcommand{\titleandyears}[2]{%
  {\textit{#1} \tabto{\rightsidetab} {#2}}\\
}

% add small space between job at same location
\newcommand{\titleandyearsSpace}[2]{%
  \vspace{0.9ex}
  {\textit{#1} \tabto{\rightsidetab} {#2}}\\
}

% remove italics on first argument
\newcommand{\titleandyearsNoItalics}[2]{%
  {{#1} \tabto{\rightsidetab} {#2}}\\
}
