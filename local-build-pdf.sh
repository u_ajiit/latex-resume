#!/bin/bash

# vars
jobname=$1
pdfout="pdfs"
extra="tex-extra"

# setup pdf output dir
if [ ! -d $pdfout ]; then
  echo "Creating default PDF output dir = $pdfout"
  mkdir -p $pdfout
fi

# setup tex-extra dir
if [ ! -d $extra ]; then
  echo "Creating default Extras output dir = $extra"
  mkdir -p $extra
fi

# create resume
if [[ -n "$jobname" ]]; then
  xelatex --jobname="$jobname" --output-directory="$pdfout/" --aux-directory="$extra/" resume.tex
else
  xelatex --output-directory="$pdfout/" --aux-directory="$extra/" resume.tex
fi
